<?php
/**
 *
 */

//Enqueue extra styles and scripts for the admin interface
add_action( 'admin_enqueue_scripts', 'falkon_add_admin_scripts' );
function falkon_add_admin_scripts() {

    //Include theme support for Font Awesome icon pack
    wp_enqueue_style( 'admin-font-awesome',"//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",array(),'4.3.0');
    wp_register_style( 'admin-style', get_stylesheet_directory_uri().'/css/admin.css',array(),filemtime( get_stylesheet_directory().'/css/admin.css') );
    wp_enqueue_style( 'admin-style' );

//    wp_enqueue_style( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css',array(),'4.0.0' );
//    wp_enqueue_script('jquery-select2','//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', array('jquery'), '4.0.0', true );

    //Local JS scripts
//    wp_enqueue_script('jquery-apps-admin',get_template_directory_uri().'/js/apps-admin.js', array('jquery'), filemtime( get_stylesheet_directory().'/js/apps-admin.js'), true );
    //Set Admin Ajax URL
//    wp_localize_script( 'jquery-apps-admin', 'the_ajax_script_app', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}