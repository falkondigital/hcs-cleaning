<?php
/**
 * cp_school.php
 * File for registering and setting up custom post type 'school'
 */

// Register Custom Post Type school
function falkon_register_cp_our_work() {

    $labels = array(
        'name'                => _x( 'Our Work', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Our Work', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Our Work', 'text_domain' ),
        'name_admin_bar'      => __( 'Our Work', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Our Work:', 'text_domain' ),
        'all_items'           => __( 'All Our Work', 'text_domain' ),
        'add_new_item'        => __( 'Add New Our Work', 'text_domain' ),
        'add_new'             => __( 'Add New Work Project', 'text_domain' ),
        'new_item'            => __( 'New Our Work', 'text_domain' ),
        'edit_item'           => __( 'Edit Our Work', 'text_domain' ),
        'update_item'         => __( 'Update Our Work', 'text_domain' ),
        'view_item'           => __( 'View Our Work', 'text_domain' ),
        'search_items'        => __( 'Search Our Work', 'text_domain' ),
        'not_found'           => __( 'No our work found', 'text_domain' ),
        'not_found_in_trash'  => __( 'No our work found in Trash', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'Our Work', 'text_domain' ),
        'description'         => __( 'Our Work projects setup for IKON Photography site', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array('title', 'author', 'thumbnail','editor'),
        'taxonomies'          => array( 'our-work-type','client' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
//        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-portfolio',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'rewrite'             => false,
        'capability_type'     => 'post',
//        'capability_type'     => 'our-work',//array('our-work','our-work'),
//        'capabilities'        => $capabilities,
//        'map_meta_cap' => true,
    );
    register_post_type( 'our-work', $args );
}
add_action( 'init', 'falkon_register_cp_our_work', 0 );


add_action('init', 'our_work_rewritex');
function our_work_rewritex(){
    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
    global $wp_rewrite;

    $our_work_page_id = (int)$falkon_option['falkon_our_work_page_id'];
//    $our_work_bi_root_page_id = (int)$falkon_option['falkon_our_work_bi'];

    $our_work_page_slug = get_relative_permalink(get_permalink($our_work_page_id));
    $our_work_page_url = untrailingslashit($our_work_page_slug);
//    $our_work_bi_root_page_slug = get_relative_permalink(get_permalink($our_work_bi_root_page_id));
//    $our_work_bi_root_page_url = untrailingslashit($our_work_bi_root_page_slug);

    $wp_rewrite->add_rewrite_tag('%cpt_name_int%', '([^/]+)', 'our-work=');
    if($our_work_page_id!=0) $wp_rewrite->add_permastruct('our_work', $our_work_page_url.'/%cpt_name_int%', false);
//    if($our_work_bi_root_page_id!=0) $wp_rewrite->add_permastruct('our_work-bi', $our_work_bi_root_page_url.'/%cpt_name_int%', false);
}

add_filter('post_type_link', 'our_work_post_permalinkx', 1, 3);
function our_work_post_permalinkx($post_link, $id = 0, $leavename) {
    global $wp_rewrite;
    $post = &get_post($id);
    if ( is_wp_error( $post ) || empty($post_link) || in_array($post->post_status, array('draft', 'pending', 'auto-draft')))
        return $post_link;

    if($post->post_type=='our-work'){
        if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
        $post_id = $post->ID;
        $get_name = $post->post_name;
        $get_name = sanitize_title($get_name);

        $newlink = $wp_rewrite->get_extra_permastruct('our_work');
        $newlink = str_replace("%cpt_name_int%", $get_name, $newlink);
        $newlink = home_url(user_trailingslashit($newlink));
        return $newlink;
    }
    return $post_link;
}


/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function falkon_save_meta_box_data_our_work( $data , $postarr ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
//    if ( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
//        return;
//    }

    // Verify that the nonce is valid.
//    if ( ! wp_verify_nonce( $_POST['myplugin_meta_box_nonce'], 'myplugin_save_meta_box_data' ) ) {
//        return;
//    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
//    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
//
//        if ( ! current_user_can( 'edit_page', $post_id ) ) {
//            return;
//        }
//
//    } else {
//
//        if ( ! current_user_can( 'edit_post', $post_id ) ) {
//            return;
//        }
//    }

    /* OK, it's safe for us to save the data now. */
    remove_action( 'wp_insert_post_data', 'set_private_categories' );
    add_settings_error(
        'missing-death-star-plans',
        'missing-death-star-plans',
        'You have not specified the location for the Death Star plans.',
        'error'
    );
    set_transient( 'settings_errors', get_settings_errors(), 30 );
    add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
    return false;

    // Make sure that it is set.
    if ( ! isset( $_POST['myplugin_new_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['myplugin_new_field'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
//add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
//add_action( 'save_post', 'myplugin_save_meta_box_data' );




// Register Custom Taxonomy - our-work-type
function create_tax_our_work_our_work_type()  {
    $labels = array(
        'name'                       => _x( 'Our Work Types', 'Taxonomy General Name', 'prolixity' ),
        'singular_name'              => _x( 'Our Work Type', 'Taxonomy Singular Name', 'prolixity' ),
        'menu_name'                  => __( 'Our Work Type', 'prolixity' ),
        'all_items'                  => __( 'All Our Work Types', 'prolixity' ),
        'parent_item'                => __( 'Parent Our Work Type', 'prolixity' ),
        'parent_item_colon'          => __( 'Parent Our Work Type:', 'prolixity' ),
        'new_item_name'              => __( 'New Our Work Type', 'prolixity' ),
        'add_new_item'               => __( 'Add New Our Work Type', 'prolixity' ),
        'edit_item'                  => __( 'Edit Our Work Type', 'prolixity' ),
        'update_item'                => __( 'Update Our Work Type', 'prolixity' ),
        'separate_items_with_commas' => __( 'Separate Our Work Type with commas', 'prolixity' ),
        'search_items'               => __( 'Search work types', 'prolixity' ),
        'add_or_remove_items'        => __( 'Add or remove work types', 'prolixity' ),
        'choose_from_most_used'      => __( 'Choose from the most used work types', 'prolixity' ),
    );
    $rewrite = array(
        'slug'                       => 'our-work-type',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'meta_box_cb'               =>  false,
        'query_var'                 => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'our-work-type',array('our-work'), $args );

    if(taxonomy_exists('our-work-type')){
        if(!term_exists('studio', 'our-work-type')){
            wp_insert_term(	'Studio', // the term
                'our-work-type', // the taxonomy
                array(
                    'slug' => 'studio'
                )
            );
        }
        if(!term_exists('furniture', 'our-work-type')){
            wp_insert_term(	'Furniture', // the term
                'our-work-type', // the taxonomy
                array(
                    'slug' => 'furniture'
                )
            );
        }
    }
}
add_action( 'init', 'create_tax_our_work_our_work_type', 0 );


// Register Custom Taxonomy - client
function create_tax_our_work_client()  {
    $labels = array(
        'name'                       => _x( 'Clients', 'Taxonomy General Name', 'prolixity' ),
        'singular_name'              => _x( 'Client', 'Taxonomy Singular Name', 'prolixity' ),
        'menu_name'                  => __( 'Work Clients', 'prolixity' ),
        'all_items'                  => __( 'All Clients', 'prolixity' ),
        'parent_item'                => __( 'Parent Client', 'prolixity' ),
        'parent_item_colon'          => __( 'Parent Client:', 'prolixity' ),
        'new_item_name'              => __( 'New Client', 'prolixity' ),
        'add_new_item'               => __( 'Add New Client', 'prolixity' ),
        'edit_item'                  => __( 'Edit Client', 'prolixity' ),
        'update_item'                => __( 'Update Client', 'prolixity' ),
        'separate_items_with_commas' => __( 'Separate Client with commas', 'prolixity' ),
        'search_items'               => __( 'Search clients', 'prolixity' ),
        'add_or_remove_items'        => __( 'Add or remove clients', 'prolixity' ),
        'choose_from_most_used'      => __( 'Choose from the most used clients', 'prolixity' ),
    );
    $rewrite = array(
        'slug'                       => 'client',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'meta_box_cb'               =>  false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'client',array('our-work'), $args );

    if(taxonomy_exists('client')){
        if(!term_exists('neville-johnson', 'client')){
            wp_insert_term(	'Neville Johnson', // the term
                'client', // the taxonomy
                array(
                    'slug' => 'neville-johnson'
                )
            );
        }
        if(!term_exists('tom-howley', 'client')){
            wp_insert_term(	'Tom Howley', // the term
                'client', // the taxonomy
                array(
                    'slug' => 'tom-howley'
                )
            );
        }
    }
}
add_action( 'init', 'create_tax_our_work_client', 0 );


function change_default_title_our_work( $title ){
    $screen = get_current_screen();
    if  ( 'our-work' == $screen->post_type ) {
        $title = 'Enter project name here';
    }
    return $title;
}
add_filter( 'enter_title_here', 'change_default_title_our_work' );

/**
 * Setup meta boxes using WPAlchemy for backend management and filter/save use on frontend.
 */
//if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_school' );
function my_metabox_styles_our_work()
{
    wp_enqueue_style( 'wpalchemy-metabox-our_work', get_stylesheet_directory_uri() . '/metaboxes/meta-our_work.css');
}

$our_work_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_our_work_meta',
    'title' => 'Our Work Meta Info',
    'types' => array('our-work'), // added only for pages and to custom post type "events"
    'context' => 'normal', // same as above, defaults to "normal"
    'priority' => 'high', // same as above, defaults to "high"
    'save_filter' => 'save_filter_our_work_data',
    'save_action' => 'save_action_our_work_meta',
    'template' => get_stylesheet_directory() . '/metaboxes/our-work-meta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_our_work_',
));

function save_filter_our_work_data($meta, $post_id){
//    var_dump('dfsdf');
//exit;
//    if ( 'our-work' == $_POST['post_type'] && !current_user_can( 'edit_our-work', $post_id )) {
//        return $post_id;
//    }

//    var_dump($_POST);
//    exit;
    if(isset($_POST['our-work_client'])) {
        $meta['client_name'] = $_POST['our-work_client'];
    }
    else
        unset($meta['client_name']);
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }

//    if(isset($meta['user_fname'])){
//        $meta['user_fname'] = 'bob';
//    }
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }
//    if(!isset($meta['address_country']) or $meta['address_country']==''){
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }
//    if(!isset($meta['client_id']) or $meta['client_id']==''){
//        add_settings_error(
//            'missing-our_work-meta-client',
//            'missing-our_work-meta-client',
//            'You must select a client from the drop down.',
//            'error'
//        );
//    }
//    var_dump($meta);
//    exit;
//    add_settings_error(
//        'missing-death-star-plans',
//        'missing-death-star-plans',
//        'You have not specified the location for the Death Star plans.',
//        'error'
//    );
//
//    add_settings_error(
//        'invalid-email',
//        '',
//        'You must define a value in the meta box.',
//        'error'
//    );

//    set_transient( 'settings_errors', get_settings_errors(), 30 );
//    return false;

//    var_dump($meta['equipment_required']);
//    exit;
    //Check for blank entries with qty = 1

//    var_dump($meta);


//exit;
//    if($meta['date_received']=='') unset($meta['date_received']);
//    if($meta['date_received']=='dd/mm/yyyy') unset($meta['date_received']);
//    if($meta['date_received']){
//        $meta['date_received'] = strtotime(str_replace('/','-',$meta['date_received']));
//    }

//    if($meta['date_required']=='') unset($meta['date_required']);
//    if($meta['date_required']=='dd/mm/yyyy') unset($meta['date_required']);
//    if($meta['date_required']){
//        $meta['date_required'] = strtotime(str_replace('/','-',$meta['date_required']));
//    }

//    if($meta['date_awarded']=='') unset($meta['date_awarded']);
//    if($meta['date_awarded']=='mm/yyyy') unset($meta['date_awarded']);
//    var_dump($meta['date_awarded']);
//    if($meta['date_awarded']){
//        $meta['date_awarded'] = strtotime("01-".str_replace('/','-',$meta['date_awarded']));
//    }
//    var_dump($meta['date_awarded']);
//exit;

//    if(isset($meta['equipment_required']) and is_array($meta['equipment_required'])){
//        foreach($meta['equipment_required'] as $key => $equipment){
//            if(count($equipment)==1 and isset($equipment['qty']))
//                unset($meta['equipment_required'][$key]);
//
//            if($equipment['equipment_id']=='' and $equipment['equipment_id_other']=='')
//                unset($meta['equipment_required'][$key]);
//            if($equipment['equipment_id'])
//            equipment_id_other
//        }
//    }
    //Re-index array
//    $meta['equipment_required'] = array_values($meta['equipment_required']);
//    var_dump($meta['equipment_required']);
//exit;

//    if(isset($meta['value']))
//        $meta['value'] = format_price($meta['value']);

//    var_dump($meta);
//exit;

    return $meta;
}


function save_action_our_work_meta($meta,$post_id){
    // Check permissions
    if ( 'our-work' == $_POST['post_type'] && !current_user_can( 'edit_posts', $post_id )) {
        return $post_id;
    }

    //Save Taxonomy to taxonomy metas
    //out-work-type
    $ourtaxonomy = $_POST['our-work_our-work-type'];
    wp_set_object_terms( $post_id, $ourtaxonomy, 'our-work-type' );

    //client
    $ourtaxonomy = $_POST['our-work_client'];
    wp_set_object_terms( $post_id, $ourtaxonomy, 'client' );
}



add_filter( 'manage_our_work_posts_columns', 'our_work_columns_filter', 10, 1 );
function our_work_columns_filter( $columns ) {
//    $column_status = array( 'our_work-status' => 'Status' );
//    $column_average_score = array( 'our_work-average-score' => 'Av Score' );
//    $column_average_time = array( 'our_work-average-time' => 'Av Time' );
//    $column_attempts_review = array( 'our_work-attempts-r' => 'Attempts Review' );
//    $column_attempts_practice = array( 'our_work-attempts-p' => 'Attempts Practice' );
//    $column_downloads = array( 'our_work-downloads' => 'Downloads' );

//    $columns = array_slice( $columns, 0, 3, true ) + $column_status + array_slice( $columns, 3, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_average_score + array_slice( $columns, 4, NULL, true );
//    $columns = array_slice( $columns, 0, 5, true ) + $column_average_time + array_slice( $columns, 5, NULL, true );
//    $columns = array_slice( $columns, 0, 6, true ) + $column_attempts_review + array_slice( $columns, 6, NULL, true );
//    $columns = array_slice( $columns, 0, 7, true ) + $column_attempts_practice + array_slice( $columns, 7, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_downloads + array_slice( $columns, 4, NULL, true );

//    unset($columns['title']);
//    unset($columns['author']);
    unset($columns['date']);
    unset($columns['comments']);
    return $columns;
}
add_action( 'manage_our_work_posts_custom_column', 'our_work_column_action', 10, 1 );
function our_work_column_action( $column ) {
    global $post;
    switch ( $column ) {

    }
}


add_action('pre_get_posts','db_query_alter_query_our_work');
function db_query_alter_query_our_work($query){

    global $post;
    global $falkon_option;

    if (!is_admin()
        and !$query->is_main_query()
        and $post->ID == (int)$falkon_option['falkon_our_work_page_id']
        and $query->get('post_type')=='our-work') {

        //Do something to main query
        //echo '<pre>'; var_dump($_GET); echo '</pre>'; die;
        //echo '<pre>'; var_dump($query); echo '</pre>'; die;

        $orderby = 'menu_order title';//"&orderby=cost&order=DESC";
        $order = "ASC";

        //Set order by if set in URL
        if (isset($_GET['orderby'])) {
            switch ($_GET['orderby']) {
                case 'client':
                    $orderby = 'meta_value';//"&orderby=cost&order=DESC";
                    $order = "ASC";
                    $metaorder = "_our_work_client_name";
                    //$query->set('meta_key',$metaorder );
                    break;
            }
        }
        $query->set('meta_key', $metaorder);

        //Create the exclusion rule
        $new_tax_query = '';

        if(isset($_GET['work-type']) and $_GET['work-type']!=''){
            $makes = explode(",", $_GET['work-type']);
            $new_tax_query = array(
                'taxonomy' => 'our-work-type',
                'field'    => 'slug',
                'terms'    => $makes,
            );
        }


        //If there is already a tax_query, 'AND' our new rule with the entire existing rule.
        $tax_query = $query->get('tax_query');
        if($tax_query=='') $tax_query = array();
        if(!empty($tax_query)) {
            $new_tax_query = array(
                'relation' => 'AND',
                $tax_query,
                $new_tax_query,
            );
        }
        else{
            $new_tax_query = array(
                'relation' => 'AND',
                $new_tax_query
            );
        }
        $query->set('tax_query',$new_tax_query);

        //If there is already a meta_query, 'AND' our new rule with the entire existing rule.
        $meta_query = $query->get('meta_query');
        //var_dump($meta_query);
        if(!empty($meta_query)) {
            $new_meta_query = array(
                'relation' => 'AND',
                $meta_query,
                $new_meta_query,
            );
        }
        else{
            $new_meta_query = array(
                'relation' => 'AND',
                $new_meta_query,
            );
        }
        //var_dump($meta_query);
        $query->set('meta_query', $new_meta_query);

        //Finally set the order and orderby from the above
        $query->set('orderby',$orderby );
        $query->set('order',$order );
//        var_dump($query);
    }
}