<?php
 global $footercta_meta;
    $footer_cta_meta = $footercta_meta->the_meta();
// Falkon Options
 global $falkon_option;
?>

<!-- BEGIN footer CTA checkbox -->
<?php


//check if meta exists              check if checkbox ↓ is set to value 1
if(is_array($footer_cta_meta) and $footer_cta_meta['show_footer_cta']=='1'){
		?>


		 <section id="pre-footer">
		   <div class="container">
		     <div class="row">
		        <div class="col-xs-12 col-sm-12 text-center">

			        <?php
			          if(isset($footer_cta_meta['ctamain'])) {
				          echo '<span>' . $footer_cta_meta['ctamain'] . '</span>';
			          }
			          else {
				          echo '<span>Speak with us, or make an enquiry with our skilled team</span>';
			          }
			        ?>
		     	    <?php if(isset($footer_cta_meta['show_footer_cta_btn'])) {?>
                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                            <br>
                        </div>
			             <a href="<?php echo esc_url( home_url( '/contact-us' ) ); ?>"><button class="contact-btn">Contact Us</button></a>
				    <?php
		              }
		            ?>

		        </div>
		     </div>
		   </div>
		 </section>
		 <?php


}
?>
<!-- END footer CTA -->

<footer>
<div id="footer">

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-2 mobile-ft-pad">
				<strong class="ft-title">About</strong>
			   <?php
			   wp_nav_menu( array(
					   'theme_location'    => 'navbar-footmenu',
					   'depth'             => 2,
					   'menu_class'        => 'footer-nav',
					   'fallback_cb'       => 'wp_bootstrap_navwalker::fallback'
			   ));
			   ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-5 mobile-ft-pad">
				<strong class="ft-title">Work For Us</strong>
				<?php if($falkon_option['falkon_contact_infomation']!='') echo '<p>'.nl2br($falkon_option['falkon_contact_infomation']).'</p>';?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-2 grey-ft-pd mobile-ft-pad">
				<?php if($falkon_option['falkon_contact_address']!='') echo '<p>'.nl2br($falkon_option['falkon_contact_address']).'</p>';?>

			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 grey-ft mobile-ft-pad">
				<p>


				<?php if($falkon_option['falkon_contact_number']!='') echo 't : <a href="tel:'. $falkon_option['falkon_contact_number'].'">' .$falkon_option['falkon_contact_number'].'</a>' ;?><br>
				<?php if($falkon_option['falkon_sec_contact_number']!='') echo 't : <a href="tel:'. $falkon_option['falkon_sec_contact_number'].'">' .$falkon_option['falkon_sec_contact_number'].'</a>' ;?><br>
				<?php if($falkon_option['falkon_contact_email']!='') echo 'e : <a href="mailto:'. $falkon_option['falkon_contact_email'].'">' .$falkon_option['falkon_contact_email'].'</a>' ;?><br>
				</p>
				<?php
				$social_links_header = $falkon_option['falkon_multicheckbox_inputs'];
				if(count($social_links_header)>0){
					foreach($social_links_header as $social_url => $value){
						//				var_dump($value);
						//var_dump($falkon_option[$social_url]);
						if($value and $falkon_option[$social_url]!='') echo '<a href="'.esc_url($falkon_option[$social_url]).'" class="sociallink2" rel="nofollow" target="_blank"><i class="ss ss1 fa fa-lg fa-custom-'.$social_url.'"></i><i class="ss ss2 fa fa-lg fa-custom-'.$social_url.'"></i></a>';
					}
				}
				?>

			</div>
		</div>
	</div>
</div>
</footer>

<div id="ft-logo">
	<div class="container">
   <div class="row">
	<div class="col-xs-6 col-sm-2">
	   <img src="<?php bloginfo('template_url');?>/images/ft-acc/safe-contractor.png" title="Safe Contractor" alt="Safe Contractor Logo" data-retinaX>
	</div>
	   <div class="col-xs-6 col-sm-2">
		   <img src="<?php bloginfo('template_url');?>/images/ft-acc/chas.png" title="Chas Contractor" alt="Chas Contractor Logo" data-retinaX>
	   </div>
	<div class="col-xs-6 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/fwc.png" title="Federation Of Window Cleaners" alt="Federation Of Window Cleaners Logo" data-retinaX>
	</div>
	<div class="col-xs-6 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/iosh.png" title="Iosh" alt="Iosh Logo" data-retinaX>
	</div>
	<div class="col-xs-6 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/bwca.png" title="British Window Cleaning Acc" alt="British Window Cleaning Acc Logo" data-retinaX>
	</div>
	<div class="col-xs-6 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/iraf.png" title="Iraf" alt="Iraf Logo" data-retinaX>
	</div>
  </div>
  <div class="row">
	<div class="col-xs-6 col-sm-offset-4 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/cscs.png" title="CSCS" alt="CSCS Logo" data-retinaX>
	</div>
	<div class="col-xs-6 col-sm-offset-0 col-sm-2">
		<img src="<?php bloginfo('template_url');?>/images/ft-acc/dbs.png" title="DBS" alt="DBS Logo" data-retinaX>
	</div>
  </div>
  <div class="row pd-ft">
		<div class="col-xs-12 col-sm-12 text-center">
            <span>&copy; <?php echo get_the_date(Y);?> HCS Cleaning Services Ltd. All Rights Reserved. Registered in England and Wales No: 06675884</span>
		</div>
   </div>

</div>
</div>

<script>
	jQuery(document).ready(function ($) {
		$("#clientlogo-carousel").owlCarousel({
			autoPlay: 4500, //Set AutoPlay to 3 seconds
			items : 5,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false,
			navigation:true,
			navigationText: [
				"<i class='fa fa-chevron-left fa-3x'></i>",
				"<i class='fa fa-chevron-right fa-3x'></i>"
			]
		});
		$("#clientwwd-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items : 3,
			itemsDesktop : [1199,3],
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false
		});
		$("#clientwork-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items : 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false
		});
		$("#owl-childpage").owlCarousel({
			navigation : true,
			slideSpeed : 300,
			paginationSpeed : 500,
			singleItem:true,
			autoPlay : 4000
		});
		$("#owl-testimonials").owlCarousel({
			singleItem : true,
			pagination:true,
			transitionStyle : "fade",
			autoPlay: $("#owl-testimonials > div").length > 1 ? 5000 : false
		});

	});

</script>

<?php wp_footer(); ?>
</body>
</html>
