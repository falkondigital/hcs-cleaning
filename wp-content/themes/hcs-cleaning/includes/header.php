<?php global $falkon_option; ?>
<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); ?></title>

    <?php include(locate_template('includes/blocks/google-analytics.php'));?>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="">
	<?php wp_head(); ?>

	<?php   //Favicon
	$favicon_filename = get_template_directory().'/images/ico/favicon.ico';
	if (file_exists($favicon_filename)){
		$favicon_url = get_template_directory_uri().'/images/ico/favicon.ico';
		echo '<link rel="icon" href="'.$favicon_url.'">';
	}
	?>

</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<header>
<div id="header">



	<!-- Mobile Menu -->
	<div class="visible-xs visible-sm visible-md hidden-lg">

		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">

			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar top-bar"></span>
			<span class="icon-bar middle-bar"></span>
			<span class="icon-bar bottom-bar"></span>

		</button>

        <nav class="nav-mob">
		<div class="collapse" id="menu">

          <div class="cont-pddin">
	        	<a href="tel:<?php echo $falkon_option['falkon_sec_contact_number'];?>">
			        <i class="fa fa-phone" aria-hidden="true"></i> <strong><?php echo $falkon_option['falkon_sec_contact_number'];?></strong>
		        </a>
	      </div>

<!--		   --><?php
//		      wp_nav_menu( array(
//				  'theme_location'    => 'navbar-main',
//				  'depth'             => 3,
//				  'menu_class'        => '',
//				  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
//				  'after'             => '<span class="subclicker arrw" data-toggle="dropdown"><i class="fa fa-caret-down"></i></span>',
//				  'walker'            => new wp_bootstrap_navwalker()
//			      )
//		      );
//		   ?>

			<?php
			wp_nav_menu( array(

			'theme_location'  => 'navbar-main',
			'container'       => 'div',
			'container_class' => '',
			'container_id'    => 'headmenu',
			'menu_class'      => 'mainnav',
			'menu_id'         => 'mobmenu',
			'echo'            => true,
			'fallback_cb'     => false,
			'after'           => '<i class="fa fa-chevron-down subclicker"></i>',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0
				)
			);
			?>
           <hr>
		   <div class="cont-pddin">
		        <a href="mailto:<?php echo $falkon_option['falkon_contact_email'];?>">
			        <i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $falkon_option['falkon_contact_email'];?>
			    </a>
		   </div>

	  </div>
      </nav>


	</div>




<div class="container">

	<div class="col-xs-12 col-sm-12 col-md-12">
		<nav class="navbar navbar-default">
		<div class="navbar-header">


			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home" class="navbar-brand">
				<img src="<?php echo get_stylesheet_directory_uri();?>/images/hcs-logo.png" class="site-logo">
			</a>


		</div>
		<div class="collapse navbar-collapse" id="navbar">

          <div id="contact-header" class="text-right">
	              <i class="fa fa-phone" aria-hidden="true"></i> <?php echo $falkon_option['falkon_sec_contact_number'];?>
	          &nbsp;&nbsp;&nbsp;
	          <a href="mailto:<?php echo $falkon_option['falkon_contact_email'];?>">
	              <i class="fa fa-envelope-o" aria-hidden="true"></i> <span class="email"><?php echo $falkon_option['falkon_contact_email'];?></span>
		      </a>
	      </div>

			<?php
			wp_nav_menu( array(
					'theme_location'    => 'navbar-main',
					'depth'             => 3,
					'menu_class'        => 'nav navbar-nav navbar-right',
					'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					'link_after'             => '<i class="fa fa-caret-down arrw"></i>',
					'walker'            => new wp_bootstrap_navwalker()
				)
			);
			?>

		</div><!-- /.navbar-collapse -->

	</div><!-- /.navbar-collapse -->



   </div>

</div><!-- // #header -->

</header>
