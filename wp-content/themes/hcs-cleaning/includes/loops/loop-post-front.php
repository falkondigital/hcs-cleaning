<div id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-sm-6');?>">

    <div class="newsbg">

    <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 25, '...' ); ?>

    <!-- Post Thumbnail -->
    <?php if(has_post_thumbnail($post->ID)){
        $imgSRC = get_the_post_thumbnail( $post->ID,'full', array('class'=>'hp-feed-img'));

    ?>

        <div class="newsimg">
	        <a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow" class=""><?php echo $imgSRC;?></a>
        </div>

    <?php
    }
    else{
        $args = array(
            'numberposts' => 1,
            'order'=> 'ASC',
            'post_mime_type' => 'image',
            'post_parent' =>  $post->ID,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'orderby' => 'menu_order ID'
        );
        $attachments = get_children( $args );
        if ($attachments) {
            foreach($attachments as $attachment) {
                $image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail-blog' );
                $imgSRC = '<img src="'.$image_attributes[0].'" class="hp-feed-img">';
            }
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC;?></a></div>
        <?php
        }
        else{
            $imgSRC = '<img src="'.get_template_directory_uri().'/images/default-thumbnail-265x175.jpg" width="265" height="175" class="">';
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC;?></a></div>
        <?php }
    }
    ?>

    <!-- Title & Content -->

     <div class="inner-article">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <h4><?php the_title(); ?></h4>
            </a>

            <div class="entry-meta">
                <?php flkn_news_posted_on(); ?>
            </div><!-- .entry-meta -->

            <p><?php echo $block_excerpt; ?></p>


	 </div>
    </div>
</div><!-- #post-## -->


<?php //comments_template( '', true ); ?>