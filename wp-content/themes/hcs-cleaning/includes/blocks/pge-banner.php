<?php
global $norotbanner_mb;
$nrbanner_meta = $norotbanner_mb->the_meta();
?>

<section id="pge-banner">
	<?php
    
	if (isset( $nrbanner_meta['imgurl'])) {
		echo '<div class="banner-nrt">';
		echo '<img src="' . $nrbanner_meta['imgurl'] . '" class="banner-nrt-example" alt="'. $alt_text = get_post_meta((int)$nrbanner_meta['image_id'], '_wp_attachment_image_alt', true).'"/>';
		echo '<div class="banner-nrt-title">';
		echo '<span class="title">' . $nrbanner_meta['title_line'] . '</span>';
		echo '<span class="hidden-xs hidden-sm">' . $nrbanner_meta['ban_tag'] . '</span>';
		echo '</div>';
		echo '</div>';
	}

	else {
		echo '<div class="no-img-upl">';
		echo '<img src="' . get_template_directory_uri() . '/images/banner-default.jpg" class="blog-banner-bl"/>';
//		echo '<div class="banner-nrt-title hidden-xs">';
//		echo '<span class="title">No Banner image Uploaded</span>';
//		echo '<span>On this pages settings, upload an image to appear in this area</span>';
//		echo '</div>';
		echo '</div>';
	}

//	if (isset( $nrbanner_meta['imgurl'],$nrbanner_meta['mobile_alignment'])) {
//		echo '<div class="banner-nrt">';
//		echo '<img src="' . $nrbanner_meta['imgurl'] . '" class="'.$nrbanner_meta['mobile_alignment'] .'"/>';
//		echo '<div class="banner-nrt-title">';
//		echo '<span class="title">' . $nrbanner_meta['title_line'] . '</span>';
//		echo '<span class="hidden-xs hidden-sm">' . $nrbanner_meta['ban_tag'] . '</span>';
//		echo '</div>';
//		echo '</div>';
//	}
//	elseif (isset( $nrbanner_meta['imgurl']))  {
//		echo '<div class="banner-nrt">';
//		echo '<img src="' . $nrbanner_meta['imgurl'] . '" class="banner-nrt-right"/>';
//		echo '<div class="banner-nrt-title">';
//		echo '<span class="title">' . $nrbanner_meta['title_line'] . '</span>';
//		echo '<span class="hidden-xs hidden-sm">' . $nrbanner_meta['ban_tag'] . '</span>';
//		echo '</div>';
//		echo '</div>';
//	}
//	else {
//		echo '<div class="no-img-upl">';
//		echo '<img src="' . get_template_directory_uri() . '/images/banner-default.jpg" class="blog-banner-bl"/>';
//		echo '<div class="banner-nrt-title hidden-xs">';
//		echo '<span class="title">No Banner image Uploaded</span>';
//		echo '<span>On this pages settings, upload an image to appear in this area</span>';
//		echo '</div>';
//		echo '</div>';
//	}
	?>
</section>

