<?php
global $genpage_meta;
$general_meta = $genpage_meta->the_meta();
if(is_array($general_meta) and $general_meta['show_main_cta']=='1')  {
	?>
	<section id="main-cta-hd">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 text-center">

					<?php if(isset($general_meta['ctaheadertag'])) {
						echo  '<span>' . $general_meta['ctaheadertag'] . '</span>';
					}
					else {
						echo '<span>Speak with us, or make an enquiry with our skilled team</span>';
					}
					?>

					<a href="#cta-form" data-toggle="collapse" data-parent="#cta-form" class="maincta-links">
						   <div class="frontpage-icon" title="Downwards CTA">
						  	  <i class="toggle-iconup fa fa-angle-down" aria-hidden="true"></i>
						   </div>
					</a>
				</div>
			</div>
			<div class="collapse" id="cta-form">
				<div class="row">
				<div class="col-xs-12 col-sm-12">
					<?php echo do_shortcode("[contact-form-7 id='76' title='Call To Action']"); ?>
				</div>
               </div>
			</div>

		</div>
	</section>

<?php  }
?>