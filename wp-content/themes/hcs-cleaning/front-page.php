<?php get_template_part('includes/header');

 global $homepage_meta;
$home_meta = $homepage_meta->the_meta();

global $genpage_meta;
$general_meta = $genpage_meta->the_meta();

global $artiste_gallery_mb;
$gallery_meta = $artiste_gallery_mb->the_meta();

 // Falkon Options
 global $falkon_option;
?>

<?php echo do_shortcode("[falkon-slider]"); ?>


<?php get_template_part('includes/blocks/main-cta'); ?>

<section id="home-tag">
<div class="container">

	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<h1><?php $genpage_meta->the_value('gen-title'); ?></h1>
			  <span class="gen-sub-title"><?php $genpage_meta->the_value('gen-tag'); ?></span>
			   <br><br>
			      <?php get_template_part('includes/loops/content-no-title'); ?>
		</div>
	</div>

</div>
</section>


<section id="blue-block">
<div class="container-fluid np">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-9 text-center vert-bp-hp">
		    <?php echo wpautop(do_shortcode($home_meta['left_editor']))  ?>
	    </div>
    </div>

    <div class="col-xs-12 col-sm-6 np">
 	    <img src="<?php $homepage_meta->the_value('img_right'); ?>" class="info-imgs" alt="<?php $alt_text = get_post_meta((int)$home_meta['image_id_right'], '_wp_attachment_image_alt', true);echo $alt_text; ?>">
    </div>

	<div class="hidden-xs col-sm-6 np">
		<img src="<?php $homepage_meta->the_value('img_left'); ?>" class="info-imgs" alt="<?php $alt_text = get_post_meta((int)$home_meta['image_id_left'], '_wp_attachment_image_alt', true);echo $alt_text; ?>">

	</div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 np">
	    <div class="col-xs-12  col-sm-12 col-md-offset-1 col-md-10 col-lg-9 text-center vert-bp-hp">
			<?php echo wpautop(do_shortcode($home_meta['right_editor']))  ?>
		</div>
	</div>

	<div class="visible-xs np">
		<img src="<?php $homepage_meta->the_value('img_left'); ?>" class="info-imgs" alt="<?php $alt_text = get_post_meta((int)$home_meta['image_id_left'], '_wp_attachment_image_alt', true);echo $alt_text; ?>">

	</div>

</div>
</section>

<section id="logo-caro">
	<div class="container text-center">

		<?php echo do_shortcode("[show-testimonials]"); ?>

		<span class="ww-title">Who We Work With</span>
        <?php echo do_shortcode("[hp_carousel]"); ?>

    </div>
</section>




<?php
$hp_news_title = $falkon_option['falkon_homepage_news_title']? $falkon_option['falkon_homepage_news_title'] : 'Latest News CHANGE ME!';
$hp_news_num = $falkon_option['falkon_homepage_news_num']? $falkon_option['falkon_homepage_news_num'] : 2;
?>
<section id="latest-news">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 text-center">
				<h3><?php echo $hp_news_title;?></h3>

				<?php
				$args = array(
					'orderby' => 'post_date',
					'order' => 'DESC',
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => $hp_news_num,
					'ignore_sticky_posts'	=> 1,
					'post__not_in' => get_option( 'sticky_posts' ),
				);

				$recent_posts = new WP_Query( $args );
				if ( $recent_posts->have_posts() ) :
					echo '<div class="row blog-list-vertical">';
					while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
						include(locate_template('includes/loops/loop-post-front.php'));
					endwhile;
					wp_reset_postdata();
					echo '</div><!--row-->';
				endif;
				?>

			</div>
		</div>
	</div>
</section>



<?php get_template_part('includes/footer'); ?>
