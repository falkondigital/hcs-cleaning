<?php
/**
 * Template Name: Contact Us
 */
get_template_part('includes/header');

global $genpage_meta;
$general_meta = $genpage_meta->the_meta();
global $norotbanner_mb;
$nrbanner_meta = $norotbanner_mb->the_meta();

// Falkon Options
global $falkon_option;
?>


<?php get_template_part('includes/blocks/pge-banner'); ?>

<?php //get_template_part('includes/blocks/main-cta'); ?>

<section id="main-cta-hd-cpg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 text-center">


					<span>Speak with us, or make an enquiry with our skilled team</span>

			</div>
		</div>
	</div>
</section>

<section id="cfpg-form" class="container">
	<div class="col-xs-12 col-sm-12">
		<?php echo do_shortcode("[contact-form-7 id='76' title='Contact form 1']"); ?>
	</div>
</section>

<section id="cfpage-content">
	<div class="container">
			<div class="col-xs-12 col-sm-12">
				<div class="text-center">
					<?php
					if (isset( $general_meta['gen-title'])) {
						echo '<h1>'.$general_meta['gen-title'].'</h1>';
					}
					?>

					<?php
					if (isset( $general_meta['gen-tag'])) {
						echo '<span class="gen-sub-title">' . $general_meta['gen-tag'] . '</span>';
					}
					?>
				</div>
			</div>
	</div>

				<?php get_template_part('includes/loops/content-no-title'); ?>

</section>




<?php get_template_part('includes/footer'); ?>
