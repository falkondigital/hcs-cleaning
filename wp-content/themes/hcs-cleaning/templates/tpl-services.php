<?php
/**
 * Template Name: Service Page
 */
get_template_part('includes/header');

global $genpage_meta;
$general_meta = $genpage_meta->the_meta();
global $norotbanner_mb;
$nrbanner_meta = $norotbanner_mb->the_meta();

// Falkon Options
global $falkon_option;
?>


<?php get_template_part('includes/blocks/pge-banner'); ?>

<?php get_template_part('includes/blocks/main-cta'); ?>

<section id="service-content">
	<div class="container">

		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="text-center">
					<?php
					if (isset( $general_meta['gen-title'])) {
						echo '<h1>'.$general_meta['gen-title'].'</h1>';
					}
					?>

					<?php
					if (isset( $general_meta['gen-tag'])) {
						echo '<span class="gen-sub-title">' . $general_meta['gen-tag'] . '</span>';
					}
					?>
				</div>
				<br><br>
				<?php get_template_part('includes/loops/content-no-title'); ?>
			</div>
		</div>

	</div>
</section>




<?php get_template_part('includes/footer'); ?>
