<?php get_template_part('includes/header'); ?>

				<div id="blg-bg">

					<section id="blog-feed">
						<div class="container">
							<div class="row blg-title">
								<h1 class="text-center">From The Archives<br/> <?php echo the_archive_title(); ?></h1>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<ul id="archive-menu">

											<li class="search-lister">
												<?php get_search_form();?>
											</li>
											<li>
												<?php wp_dropdown_categories( 'show_option_none=Categories' ); ?>
												<script type="text/javascript">

													var dropdown = document.getElementById("cat");
													function onCatChange() {
														if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
															location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
														}
													}
													dropdown.onchange = onCatChange;
												</script>
											</li>
											<li>
												<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;" class="archive-blg">
													<option value=""><?php echo esc_attr( __( 'Archives' ) ); ?></option>
													<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
												</select>
											</li>
										</ul>
									</div>
								</div>
								<div class="row">
									<?php get_template_part('includes/loops/content-blog', get_post_format()); ?>
								</div><!-- /.row -->
							</div><!-- /.col offset -->
						</div><!-- /.container -->
					</section>

				</div>


				<?php get_template_part('includes/footer'); ?>
