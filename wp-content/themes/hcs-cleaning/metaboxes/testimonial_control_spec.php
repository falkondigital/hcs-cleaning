<?php
//add_action('do_meta_boxes', 'remove_feat_image_box');

//function remove_feat_image_box() {
//    remove_meta_box( 'postimagediv', 'page', 'side' );
//}



$testimonial_control_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_$testimonial_control',
	'title' => 'Testimonial Control',
//	'types' => array('page'), // added only for pages and to custom post type "events"
	'include_post_id' => 8, // (Home Page)
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/testimonial_control.php',
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_testimonial_control_'
));