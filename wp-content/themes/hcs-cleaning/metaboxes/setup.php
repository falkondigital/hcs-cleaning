<?php

include_once get_template_directory() . '/metaboxes/wpalchemy/metabox.php';
include_once get_template_directory() . '/metaboxes/wpalchemy/mediaaccess.php';
//include_once get_template_directory() . '/metaboxes/wpalchemy/mediaaccess_old.php';

//include_once get_template_directory() . '/metaboxes/content-block-spec.php';
//$wpalchemy_media_access = new WPAlchemy_MediaAccess();


if (is_admin()) add_action('admin_enqueue_scripts', 'metabox_style');

function metabox_style() {
	wp_enqueue_style('wpalchemy-metabox', get_stylesheet_directory_uri() . '/metaboxes/meta.css');
}

$wpalchemy_media_access = new WPAlchemy_MediaAccess();
//$wpalchemy_media_access_old = new WPAlchemy_MediaAccess_Old();

/* eof */