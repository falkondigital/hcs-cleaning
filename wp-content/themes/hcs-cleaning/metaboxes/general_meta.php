<div class="my_meta_control">

	<!-- START Main CTA tickbox -->
	<?php $mb->the_field('show_main_cta'); ?>

	<label for="show_main_cta"> Show main call to action?</label>
	<span>
		<strong>Please Note:</strong> This will NOT be shown on blog post's and is automatically shown on the contact us page.
	</span>
	<p><input type="checkbox" id="show_main_cta" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/>Tick this box if you want the main call to action to appear near the top of the page.</p>
	<!--END tickbox meta-->

	<p>
		<span>Enter some text for the CTA</span>
		<span><em>Default text if CTA box is ticked : "Speak with us, or make an enquiry with our skilled team"</em></span>
		<input type="text" name="<?php $mb->the_name('ctaheadertag'); ?>" value="<?php $mb->the_value('ctaheadertag'); ?>"/>
	</p>

	<br/><br/>

	<p>
		<span>Styled page title</span>
		<span><em>This pre-styled text will appear differently depending on the page template</em></span>
		<input type="text" name="<?php $mb->the_name('gen-title'); ?>" value="<?php $mb->the_value('gen-title'); ?>"/>
	</p>
	<p>
		<span>Optional styled page sub-title</span>
		<span><em>This pre-styled text will appear differently depending on the page template</em></span>
		<input type="text" name="<?php $mb->the_name('gen-tag'); ?>" value="<?php $mb->the_value('gen-tag'); ?>"/>
	</p>
	<br>
	<br>

</div>