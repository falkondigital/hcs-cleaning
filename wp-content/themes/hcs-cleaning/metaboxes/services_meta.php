<?php global $wpalchemy_media_access; ?>

<div class="my_meta_control" xmlns="http://www.w3.org/1999/html">

<label>How to display this block:</label>
	<span>Insert the following shortcode into the content editor above:  [blue_block]</span>
<div class="img-content-hold">

	   <div class="img-content-iner">

		   <?php $mb->the_field('leftserv_editor'); ?>
		   <?php
		   $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
		   $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
		   $settings = array(
			   'quicktags' => array(
				   'buttons' => 'p,em,strong,link,ul,ol',
			   ),
			   /*'quicktags' => true,*/
			   'tinymce' => true,
			   'media_buttons'	=> false,
			   'textarea_name'	=> $mb->get_the_name(),
			   'textarea_rows'	=> 14,
			   'teeny'			=> true,
		   );

		   wp_editor($content, $id, $settings);

		   ?>

	   </div>
	   <div class="img-content-iner">

			<?php $mb->the_field('imgserv_right'); ?>
			<?php
			?><br><br>
			<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="side-img">
		    <p>Upload and insert images to the post. </p>
			<?php $mb->the_field('imgserv_right'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
			<?php $mb->the_field('image_id'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
			<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>

	   </div>
</div>



	&nbsp;
</div>