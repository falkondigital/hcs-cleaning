<?php

$services_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_services_meta',
	'title' => 'Blue Background Section',
	'include_template' => array('templates/tpl-services.php'),
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/services_meta.php'
));

/* eof */