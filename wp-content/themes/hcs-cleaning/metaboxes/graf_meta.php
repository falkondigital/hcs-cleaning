<?php global $genpage_meta; ?>
<?php global $wpalchemy_media_access; ?>

<div class="my_meta_control" xmlns="http://www.w3.org/1999/html">


<div class="img-content-hold">


	   <div class="img-content-graf">

		   <p>Upload a image here and control it with the shortcode : [graffeti_block]</p>
			<?php $mb->the_field('img_graf'); ?>
			<?php
			?><br>
			<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="side-img">
		    <p>Upload and insert images to the post. </p>
			<?php $mb->the_field('img_graf'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
			<?php $mb->the_field('image_id'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
			<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>

	   </div>
</div>




	&nbsp;
</div>