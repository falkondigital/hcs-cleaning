jQuery(document).ready(function($){
    //check_upload_button();

    $( '.flkn-delete-frontpage' ).on( 'click', function(e)
    {
        e.preventDefault();
        if (confirm("Are you sure?")) {
            // your deletion code
            var img_gal = $('.bannerimg');
            img_gal.attr("src", '');
            //img_gal.addClass( 'default-image');
            $('#banner-preview').addClass('hidden');
            $('#table-hide-fade').addClass('hidden');
            $('#image_id').val('');
            check_upload_button();
        }
        return false;
    });

    function check_upload_button(){
        $imageList = $( '#banner-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' ),
        uploadedImageID = $('#image_id').val();
    }

	var _custom_media = true,
    _orig_send_attachment = wp.media.editor.send.attachment;

	$('.upload_frontpage_button').click(function(e) {
        $uploadButton = $( this )
        $imageList = $( '#banner-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' );
        var frontpageworkflow = wp.media({
		title: 'Select block image',
		// use multiple: false to disable multiple selection
		multiple: false,
		button: {
			text: 'Add selected image'
		},
		library: {
			type: 'image'
		}
    });

var image_array = [];

function getAttachment(attachment) {
	attachment = attachment.toJSON();
    //console.log(attachment);
	return {id:attachment.id,url:attachment.url,thumbnail:attachment.sizes.thumbnail};
}

function select() {
	// use this to unbind the event
	// frontpageworkflow.off("select");
	// get all selected images (id/url attributes only)

    var selection = frontpageworkflow.state().get( 'selection' ).toJSON(),
        uploaded = $imageList.children().length;
    ids = _.pluck( selection, 'id' );


    var selection = frontpageworkflow.state().get( 'selection' ).toJSON();
    selection = _.filter( selection, function( attachment )
    {
        return $imageList.children( 'li#img_id_' + attachment.id ).length == 0;

    } );

    selection.map( function( attachment ) {
        var thumb_url = attachment.sizes.hasOwnProperty('banner-image-thumb')? attachment.sizes.thumbnail.url : attachment.url ;
        $('.bannerimg').attr("src", thumb_url);
        $('#banner-preview').removeClass('hidden');
        $('#image_id').val(attachment.id);

    });
}

function reset() {
	// called when dialog is closed by "close" button / "ESC" key
	// use the following line unbind the event
	// frontpageworkflow.off("select");
}

// bind event handlers
frontpageworkflow.on("select",select);
frontpageworkflow.on("escape",reset);

// open the dialog
frontpageworkflow.open();
	});
});