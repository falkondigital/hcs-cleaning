<?php global $footercta_meta; ?>

<div class="my_meta_control">
    <!-- START tickbox meta-->
	<label for="<?php $mb->the_name(); ?>">Show Pre Footer?</label>
		<?php $mb->the_field('show_footer_cta'); ?>

	<p class="description"><input type="checkbox" id="show_footer_cta" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick this box if you want to display blue pre footer towards the bottom of the page.</p>
	<!--END tickbox meta-->

	<br/>
	<label>Pre Footer Text</label>
	<span>Enter some text to show in the pre footer, by default "Speak with us, or make an enquiry with our skilled team" will show.</span>
	<p>
		<span>Enter a title</span>
		<input type="text" name="<?php $mb->the_name('ctamain'); ?>" value="<?php $mb->the_value('ctamain'); ?>"/>
	</p>

	<?php $mb->the_field('show_footer_cta_btn'); ?>
	<p class="description"><input type="checkbox" id="show_footer_cta_btn" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick this box if you want to display a button to link to the contact us page.</p>
	<!--END tickbox meta-->



</div>

