<?php

$homepage_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_homepage_meta',
	'title' => 'Blue Background Section',
	'include_post_id' => 8,
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/home_meta.php'
));

/* eof */