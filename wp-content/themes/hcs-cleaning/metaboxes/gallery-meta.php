<div class="gallery_meta_control">
    <?php $max_image_num = 10;?>
	<p>Here you can upload images for the work carousel area. You can upload an maximum of <?php echo $max_image_num._n(' image',' images',$max_image_num);?>.</p>

	<?php $mb->the_field('artiste_gal'); ?>
    <ul id="imggal" data-max_file_uploads="<?php echo $max_image_num;?>">
        <?php
        if($mb->get_the_value()!=null){
            foreach($mb->get_the_value() as $image_id){
                $image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' );
                echo '
                <li id="img_id_'.$image_id.'"><input type="hidden" name="_artiste_gallery[artiste_gal][]" value="'.$image_id.'"/><img src="'.$image_url[0].'" />
                <div class="flkn-image-bar">
                    <a title="Edit" class="flkn-edit-file" href="'.admin_url("post.php?post=$image_id&amp;action=edit").'" target="_blank">Edit</a> |
                    <a title="Delete" class="flkn-delete-file submitdelete" href="#">Delete</a>
                </div>
                </li>';
            }
        }
        ?>
    </ul>
    <input id="upload_logo_button" type="button" class="button alignleft" value="Upload Images" />
    <a title="Delete All Images" class="flkn-delete-all-files submitdelete alignright" href="#" >Delete All Images</a>
    <?php /*<span class="description">Upload artiste images for use in the gallery section.</span>*/ ?>


    <script type="text/javascript">
        jQuery(function($)
        {
            $('#imggal').sortable(
                {
                    stop: function(){
                        $('#sort_warning').show();
                    }

                });
        });
    </script>





</div>