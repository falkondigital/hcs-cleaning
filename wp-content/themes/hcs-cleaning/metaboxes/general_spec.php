<?php

$genpage_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_genpage_meta',
	'title' => 'Page Titles & CTA',
	'types' => array('page'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'exclude_post_id'   =>  array(23),
	'template' => get_stylesheet_directory() . '/metaboxes/general_meta.php'
));

/* eof */