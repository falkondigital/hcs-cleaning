<?php

$redirect_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_redirect_meta',
	'title' => 'Optional Redirect',
    'types' => array('page'), // added only for pages and to custom post type "events"
	'context' => 'side', // same as above, defaults to "normal"
	'priority' => 'low', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/redirect_meta.php'
));

/* eof */