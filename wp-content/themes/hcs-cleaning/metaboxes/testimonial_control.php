<?php
global $testimonial_control_mb;
$tml_control = $testimonial_control_mb->the_meta();
?>

<div class="banner_meta_control">

	<span>
		 Select multiple or a single testimonial to be displayed.<br>
		 To select a <strong>Single</strong> static single testimonial, click to highlight one and update the page.<br>
		 To select <strong>Mulitple</strong> testimonials, hold ctrl(control) and click the specific testimonials, then update the page.<br><br>
		 <strong>REMEMBER</strong> - <em>Testimonials are displayed with the shortcode [show-testimonials]. This won't be needed on the home page.</em>
	</span>

    <br><br>

	<?php $mb->the_field('testimonial_select', WPALCHEMY_FIELD_HINT_SELECT_MULTI); ?>

	<select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" multiple="multiple" style="width:50%; padding:10px; height:250px;">

		<?php
		$names = $mb->get_the_value();


		$args = array(
			'post_type'        => 'testimonial',
			'posts_per_page' => '9999'
		);
		$posts_array = get_posts( $args );
		foreach ( $posts_array as $tst_meta ) : setup_postdata( $tst_meta );

			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta($tst_meta->ID);

			echo '<option class="'.$posts_array.'-option" value="' . $tst_meta->ID . '" '. (in_array($tst_meta->ID, $tml_control['testimonial_select'])?'selected':'') .'>' . $testimonial_meta['name'].' , <strong>' .$testimonial_meta['company'] . '</strong>' . '</option>\n';


			?>

		<?php endforeach;?>

	</select>


    <?php

	wp_reset_postdata();?>


    <br><Br>


    <?php

    if (isset( $tml_control['testimonial_select'])) {

	    echo '<strong>Active Testimonials</strong>';

	    echo '<ol>';

	    $testimonial_array = $tml_control['testimonial_select'];

	    if ( is_array( $testimonial_array ) ) {
		    foreach ( $testimonial_array as $testimonial_slide ) {


			    global $testimonial_mb;
			    $testimonial_meta = $testimonial_mb->the_meta( $testimonial_slide );

			    echo '<li>' . $testimonial_meta['name'] . ' , <strong>' . $testimonial_meta['company'] . '</strong></li>';
		    }
	    }
	    echo '</ol>';

    }
    else {
	    echo 'You haven\'t selected any testimonials to display on this page. Select them above and display them with the shortcode.';
    }
?>

</div>