<?php global $genpage_meta; ?>
<?php global $wpalchemy_media_access; ?>

<div class="my_meta_control" xmlns="http://www.w3.org/1999/html">

    <label>Case Study One</label>

    <span>PDF Link</span>
	<input type="text" name="<?php $mb->the_name('pdf_link'); ?>" value="<?php $mb->the_value('pdf_link'); ?>"/>

	<div>
	<?php $mb->the_field('case_one'); ?>
	<?php
	?><br><br>
	<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>">
	<p>Upload and insert images to the post. </p>
	<?php $mb->the_field('case_one'); ?>
	<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
	<?php $mb->the_field('image_id'); ?>
	<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
	<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
	</div>

	<span>Case Study Name</span>
	<input type="text" name="<?php $mb->the_name('case_one_name'); ?>" value="<?php $mb->the_value('case_one_name'); ?>"/>
	<span>PDF Size</span>
	<input type="text" name="<?php $mb->the_name('case_one_size'); ?>" value="<?php $mb->the_value('case_one_size'); ?>"/>

    <hr>

	<label>Case Study Two</label>

	<span>PDF Link</span>
	<input type="text" name="<?php $mb->the_name('pdf_link2'); ?>" value="<?php $mb->the_value('pdf_link2'); ?>"/>

	<div>
		<?php $mb->the_field('case_two'); ?>
		<?php
		?><br><br>
		<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>">
		<p>Upload and insert images to the post. </p>
		<?php $mb->the_field('case_two'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
		<?php $mb->the_field('image_id'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
		<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
	</div>

	<span>Case Study Name</span>
	<input type="text" name="<?php $mb->the_name('case_two_name'); ?>" value="<?php $mb->the_value('case_two_name'); ?>"/>
	<span>PDF Size</span>
	<input type="text" name="<?php $mb->the_name('case_two_size'); ?>" value="<?php $mb->the_value('case_two_size'); ?>"/>

	<hr>

	<label>Case Study Three</label>

	<span>PDF Link</span>
	<input type="text" name="<?php $mb->the_name('pdf_link3'); ?>" value="<?php $mb->the_value('pdf_link3'); ?>"/>

	<div>
		<?php $mb->the_field('case_three'); ?>
		<?php
		?><br><br>
		<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>">
		<p>Upload and insert images to the post. </p>
		<?php $mb->the_field('case_three'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
		<?php $mb->the_field('image_id'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
		<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
	</div>

	<span>Case Study Name</span>
	<input type="text" name="<?php $mb->the_name('case_three_name'); ?>" value="<?php $mb->the_value('case_three_name'); ?>"/>
	<span>PDF Size</span>
	<input type="text" name="<?php $mb->the_name('case_three_size'); ?>" value="<?php $mb->the_value('case_three_size'); ?>"/>

	<hr>

	<label>Case Study Four</label>

	<span>PDF Link</span>
	<input type="text" name="<?php $mb->the_name('pdf_link4'); ?>" value="<?php $mb->the_value('pdf_link4'); ?>"/>

	<div>
		<?php $mb->the_field('case_four'); ?>
		<?php
		?><br><br>
		<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>">
		<p>Upload and insert images to the post. </p>
		<?php $mb->the_field('case_four'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
		<?php $mb->the_field('image_id'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
		<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
	</div>

	<span>Case Study Name</span>
	<input type="text" name="<?php $mb->the_name('case_four_name'); ?>" value="<?php $mb->the_value('case_four_name'); ?>"/>
	<span>PDF Size</span>
	<input type="text" name="<?php $mb->the_name('case_four_size'); ?>" value="<?php $mb->the_value('case_four_size'); ?>"/>

	&nbsp;
</div>