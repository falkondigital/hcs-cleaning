<div class="my_meta_control">
	<p>Enter all the specific testimonial information below.</p>

    <table class="form-table">
        <tbody>
        <tr valign="top">
	        <td style="width: 25%;" class="labelfield"><label for="">Testimonial:</label></td>
	        <td>
	        <?php $mb->the_field('description'); ?>
	        <?php
	        $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
	        $id = sanitize_key($mb->get_the_name()); //Needs to not have [] in the id, only letters and underscores
	        $settings = array(
		        'quicktags' => array(
			        'buttons' => 'em,strong,link,ul,ol',
		        ),
		        /*'quicktags' => true,*/
		        'tinymce' => true,
		        'media_buttons'	=> false,
		        'textarea_name'	=> $mb->get_the_name(),
		        'textarea_rows'	=> 5,
		        'teeny'			=> true,
	        );
	        wp_editor($content, $id,$settings);
	        ?>
	        </td>
        </tr>
        <tr valign="top">
	        <?php $mb->the_field('name'); ?>
            <td style="width: 25%;" class="labelfield"><label for="">Name:</label></td>
            <td>
            <input type="text" class="text-normal" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>" />
            <br /><span class="description">Enter the author name of the testimonial.</span>
            </td>
        </tr>
<!--        <tr valign="top">-->
<!--	        --><?php //$mb->the_field('position'); ?>
<!--	        <td style="width: 25%;" class="labelfield"><label for="">Position in Company:</label></td>-->
<!--	        <td>-->
<!--		        <input type="text" class="text-normal" name="--><?php //$metabox->the_name(); ?><!--" value="--><?php //$metabox->the_value(); ?><!--" />-->
<!--		        <br /><span class="description">Enter the author's position in their company.</span>-->
<!--	        </td>-->
<!--        </tr>-->
<!--        <tr valign="top">-->
<!--	        --><?php //$mb->the_field('company'); ?>
<!--	        <td style="width: 25%;" class="labelfield"><label for="">Company Name:</label></td>-->
<!--	        <td>-->
<!--		        <input type="text" class="text-normal" name="--><?php //$metabox->the_name(); ?><!--" value="--><?php //$metabox->the_value(); ?><!--" />-->
<!--		        <br /><span class="description">Enter the author company name.</span>-->
<!--	        </td>-->
<!--        </tr>-->

    </tbody>
    </table>
</div>
