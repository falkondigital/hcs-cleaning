<?php

$graf_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_graf_meta',
	'title' => 'Graffiti Image Section',
	'include_post_id' => 248,
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/graf_meta.php'
));

/* eof */