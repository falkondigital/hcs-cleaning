

<div class="my_meta_control" xmlns="http://www.w3.org/1999/html">

    <tr>
        <?php $mb->the_field('redirect_to_sub_page'); ?>
        <th scope="row"><label for="<?php $mb->the_name(); ?>">Redirect this page</label></th>
        <td>
            <label for="redirect_to_sub_page"><input type="checkbox" id="redirect_to_sub_page" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick to Redirect</label><br/>
            <p class="description">Tick this box if you want this page to redirect the user to the next available sub-page - based on the order of menu_order variable.</p>
        </td>
    </tr>


</div>