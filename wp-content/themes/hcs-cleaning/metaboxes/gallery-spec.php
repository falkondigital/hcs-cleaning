<?php



if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_artiste_gallery' );

function my_metabox_styles_artiste_gallery()
{
	wp_enqueue_style( 'wpalchemy-metabox-artiste-gallery', get_stylesheet_directory_uri() . '/metaboxes/meta-artiste-gallery.css');
	//wp_enqueue_script( 'jquery-ui-spinner',array( 'jquery' ) );
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_enqueue_script('media-upload');
//    wp_enqueue_script('wptuts-upload');
    wp_enqueue_media();
    wp_enqueue_script( 'mike-upload', get_template_directory_uri() .'/js/mike-upload.js', array('jquery','media-upload','thickbox') );
}


//if ( is_admin()) add_action('init', 'load_jquery_ui');

$artiste_gallery_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_artiste_gallery',
	'title' => 'Carousel',
//	'types' => array('page'), // added only for pages and to custom post type "events"
	'include_post_id' => 8, // (Home Page)
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	//'init_action' => 'remove_default_divs_artiste',
	//'save_filter' => 'meta_artiste_save_filter_func',
	//'save_action'	=>	'save_artiste_data',
	'template' => get_stylesheet_directory() . '/metaboxes/gallery-meta.php',
	//'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_artiste_gallery_'
));

/* eof */