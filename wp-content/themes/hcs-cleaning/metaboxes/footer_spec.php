<?php

$footercta_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_footercta_meta',
	'title' => 'Pre Footer (blue area)',
	'types' => array('page', 'post'), // added only for pages and to custom post type "events"
	'exclude_post_id'   =>  array(23,28),
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/footer_meta.php'
));

/* eof */