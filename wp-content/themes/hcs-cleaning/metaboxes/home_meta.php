<?php global $genpage_meta; ?>
<?php global $wpalchemy_media_access; ?>

<div class="my_meta_control" xmlns="http://www.w3.org/1999/html">


<label>Row One</label>
<div class="img-content-hold">

	   <div class="img-content-iner">

<!--		  --><?php //$mb->the_field('left_editor');
//		    $editorID = 'first-hp';
//
//		    $settings = array(
//		 	  'textarea_rows' => '12',
//			  'media_buttons' => true,
//			  'tabindex' => 1,
//			  'textarea_name' => $mb->get_the_name(),
//		   );
//
//		   // need to html_entity_decode() the value b/c WP Alchemy's get_the_value() runs the data through htmlentities()
//		   wp_editor( html_entity_decode( $mb->get_the_value() ),  $editorID , $settings );
//
//		   ?>
		   <?php $mb->the_field('left_editor'); ?>
		   <?php
		   $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
		   $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
		   $settings = array(
			   'quicktags' => array(
				   'buttons' => 'p,em,strong,link,ul,ol',
			   ),
			   /*'quicktags' => true,*/
			   'tinymce' => true,
			   'media_buttons'	=> false,
			   'textarea_name'	=> $mb->get_the_name(),
			   'textarea_rows'	=> 14,
			   'teeny'			=> true,
		   );

		   wp_editor($content, $id, $settings);

		   ?>

	   </div>
	   <div class="img-content-iner">

			<?php $mb->the_field('img_right'); ?>
			<?php
			?><br><br>
			<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="side-img">
		    <p>Upload and insert images to the post. </p>
			<?php $mb->the_field('img_right'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
			<?php $mb->the_field('image_id_right'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
			<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>

	   </div>
</div>

<label>Row Two</label>
<div class="img-content-hold">

	   <div class="img-content-iner">
			<?php $mb->the_field('img_left'); ?>
			<?php
			?><br><br>
			<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="side-img">
		    <p>Upload and insert images to the post. </p>
			<?php $mb->the_field('img_left'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
			<?php $mb->the_field('image_id_left'); ?>
			<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
			<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
	   </div>

	   <div class="img-content-iner">

		  <?php $mb->the_field('right_editor'); ?>
		  <?php
		  $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
		  $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
		  $settings = array(
			  'quicktags' => array(
				  'buttons' => 'p,em,strong,link,ul,ol',
			  ),
			  /*'quicktags' => true,*/
			  'tinymce' => true,
			  'media_buttons'	=> false,
			  'textarea_name'	=> $mb->get_the_name(),
			  'textarea_rows'	=> 14,
			  'teeny'			=> true,
		  );

		  wp_editor($content, $id, $settings);

		  ?>
	   </div>

</div>


	&nbsp;
</div>