<?php

$casestudy_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_casestudy_meta',
	'title' => 'Case Studies',
	'include_post_id' => 144,
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/case_studies_meta.php'
));

/* eof */