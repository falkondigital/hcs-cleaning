jQuery(document).ready(function($){

    check_upload_button();
    $( '#imggal' ).on( 'click', '.flkn-delete-file', function(e)
    {
        e.preventDefault();
        var $this = $( this );
        $parent = $this.parent();
        $parent.parent().remove();
        //console.log($parent);
        //return false;
        check_upload_button();
    });

    $( '.flkn-delete-all-files' ).on( 'click', function(e)
    {
        e.preventDefault();
        if (confirm("Are you sure?")) {
            // your deletion code
            var img_gal = $('#imggal');
            img_gal.empty();
        }
        return false;
    });

    function check_upload_button(){
        $imageList = $( '#imggal' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' ),
        uploaded = $imageList.children().length;
//        console.log(maxFileUploads);
//        console.log(uploaded);
        $('#upload_logo_button').show();
        if(uploaded >= maxFileUploads)$('#upload_logo_button').hide();

        $('.flkn-delete-all-files').show();
        if(uploaded<=0)$('.flkn-delete-all-files').hide();
    }


	var _custom_media = true,
    _orig_send_attachment = wp.media.editor.send.attachment;

	$('#upload_logo_button').click(function(e) {
       // console.log(_orig_send_attachment);
        $uploadButton = $( this )
        $imageList = $( '#imggal' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' );
//        console.log(maxFileUploads);
        var workflow = wp.media({
		title: 'Select the images',
		// use multiple: false to disable multiple selection
		multiple: 'add',
		button: {
			text: 'Add selected images'
		},
		library: {
			type: 'image'
		}
    });

var image_array = [];

function getAttachment(attachment) {
	attachment = attachment.toJSON();
    //console.log(attachment);
	return {id:attachment.id,url:attachment.url,thumbnail:attachment.sizes.thumbnail};
}

function select() {
	// use this to unbind the event
	// workflow.off("select");
	// get all selected images (id/url attributes only)

    var selection = workflow.state().get( 'selection' ).toJSON(),
        uploaded = $imageList.children().length;
    //console.log(uploaded);
    //console.log(selection.length);




    //console.log(selection);
    ids = _.pluck( selection, 'id' );

    //console.log(ids);








    var selection = workflow.state().get( 'selection' ).toJSON();
    //console.log(selection);
    selection = _.filter( selection, function( attachment )
    {
        //console.log($imageList.children( 'li#img_id_' + attachment.id ).length);
        return $imageList.children( 'li#img_id_' + attachment.id ).length == 0;

    } );
    //console.log(selection);





    //image_array = (workflow.state().get('selection').map(getAttachment))
   // console.log(workflow.state().get('selection').map(getAttachment));
    //console.log(_orig_send_attachment);
    //attachment.id
   // $("#logo_url").val(attachment.id);
   // console.log(image_array);



    //var selection = workflow.state().get('selection');

//    console.log(uploaded);
//    console.log(selection.length);

    if ( maxFileUploads > 0 && ( uploaded + selection.length ) > maxFileUploads )
    {
        if ( uploaded < maxFileUploads )
            selection = selection.slice( 0, maxFileUploads - uploaded );
        alert( "You can only have a maximum of "+maxFileUploads+" images." );

    }
//    console.log(selection.length);

    selection.map( function( attachment ) {
        //attachment = attachment.toJSON();
//        console.log(attachment);
//        console.log(attachment.sizes.hasOwnProperty('thumbnail'));
        var thumb_url = attachment.sizes.hasOwnProperty('thumbnail')? attachment.sizes.thumbnail.url : attachment.url ;
        //$("#dataTable").attr('data-timer') !== "undefined"
        $("#imggal").append('<li id="img_id_'+attachment.id+'"><input type="hidden" name="_artiste_gallery[artiste_gal][]" value="'+attachment.id+'"/><img src="'+thumb_url+'" />' +
            '<div class="flkn-image-bar">'+
                '<a title="Edit" class="flkn-edit-file" href="'+attachment.editLink+'" target="_blank">Edit</a> |' +
                '<a title="Delete" class="flkn-delete-file submitdelete" href="#" data-attachment_id="1792">Delete</a>' +
            '</div>' +
            '</li>');
    });
    check_upload_button();

}

function reset() {
	// called when dialog is closed by "close" button / "ESC" key
	// use the following line unbind the event
	// workflow.off("select");
}

// bind event handlers
workflow.on("select",select);
workflow.on("escape",reset);

// open the dialog
workflow.open();
	});
});