(function ($) {

	"use strict";

	$(document).ready(function() {

		// Comments

		$(".commentlist li").addClass("panel panel-default");
		$(".comment-reply-link").addClass("btn btn-default");

		// Forms

		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		// You can put your own code in here



        $('.maincta-links').click(function(){
            var collapsed=$(this).find('i').hasClass('fa fa-angle-down');
               $('.maincta-links').find('i').removeClass('fa-angle-up');
               $('.maincta-links').find('i').addClass('fa fa-angle-down');
            if(collapsed)
                $(this).find('i').toggleClass('fa-angle-down fa-2x fa-angle-up fa-2x')
        });

        // on click mobile menu
        //$('#menu [data-toggle=dropdown]').on('click', function(event) {
        //    event.preventDefault();
        //    event.stopPropagation();
        //    $(this).parent().siblings().removeClass('open');
        //    $(this).parent().toggleClass('open');
        //});


        /* MOBILE COLLAPSE MENU */
        (function(jQuery) {
            jQuery.fn.collapsable = function(options) {
                // iterate and reformat each matched element
                return this.each(function() {
                    // cache this:
                    var obj = jQuery(this);
                    var tree = jQuery('.mainnav');
                    obj.click(function(){
                        tree.toggle();
                    });
                    jQuery(window).resize(function(){
                        if ( jQuery(window).width() <= 768 ){tree.attr('style','');};
                    });
                });
            };
        })(jQuery);

        jQuery(document).ready(function($){
            $('#menudropper').collapsable();

            $('.subclicker').click(function(e){
                //e.preventDefault();
                //$(".sub-menu", this).slideToggle("fast");
                $(this).next('.sub-menu').toggleClass('active');
                //$(this).parent('.menu-item').toggleClass('active');
                $(this).toggleClass('fa-chevron-down fa-chevron-up');
            });
        });

	});


}(jQuery));
